import argparse
import re
import socket
import struct
from multiprocessing.dummy import Pool as ThreadPool
import ssl
import itertools

dns_packet = b"\x00\x02\x01\x00\x00\x01\x00\x00" \
             b"\x00\x00\x00\x00\x02\x76\x6b\x03" \
             b"\x63\x6f\x6d\x00\x00\x01\x00\x01"

tcp_protocols = {'pop3': (b'NOOP', re.compile(b'^\+')),
                 'dns': (struct.pack('!H', len(dns_packet)) + dns_packet, re.compile(b'^.{2}\x00\x02')),
                 'http': (b'\0', re.compile(b'^HTTP')),
                 'smtp': (b'EHLO', re.compile(b'^\d{3}'))
                 }

udp_protocols = {'dns': (dns_packet, re.compile(b'\x00\x02')),
                 'sntp': (b'\x1b' + 47 * b'\0', re.compile(b'^\x1c'))
                 }


def scan_tcp_port(port, ssl_needed=False):
    result = ''
    for protocol, payload in tcp_protocols.items():
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            if ssl_needed:
                sock = ssl.wrap_socket(sock)
            sock.settimeout(1)
            try:
                sock.connect((host, port))
                result = f'{port} is open TCP port'
                try:
                    sock.send(payload[0])
                    if re.match(payload[1], sock.recv(1024)):
                        print(result + f' with protocol {protocol}')
                        return
                except socket.error:
                    continue
            except (socket.timeout, socket.error):
                return
    print(result)


def scan_udp_port(port, ssl_needed=False):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        if ssl_needed:
            sock = ssl.wrap_socket(sock)
        sock.settimeout(1)
        for protocol, payload in udp_protocols.items():
            sock.sendto(payload[0], (host, port))
            try:
                received = sock.recv(1024)
                result = f'{port} is open UDP port'
                if re.match(payload[1], received):
                    print(result + f' with protocol {protocol}')
                    return
                print(result)
            except (socket.timeout, socket.error):
                continue


def create_arg_parser():
    parser = argparse.ArgumentParser(description='TCP and UDP port scanner')
    parser.add_argument('host', type=str, help='ip address or a host name')
    parser.add_argument('--start', '-s', type=int, help='start of range', default=1)
    parser.add_argument('--end', '-e', type=int, help='end of range', default=500)
    parser.add_argument('--tcp', '-t', action='store_true', help='scan tcp ports')
    parser.add_argument('--udp', '-u', action='store_true', help='scan udp ports')
    parser.add_argument('--ssl', action='store_true', help='should use ssl wrapper or not')
    return parser


if __name__ == '__main__':
    args = create_arg_parser().parse_args()
    host = args.host
    pool = ThreadPool(50)
    if args.tcp:
        pool.starmap(scan_tcp_port, zip(range(args.start, args.end), itertools.repeat(args.ssl)))
    if args.udp:
        pool.starmap(scan_udp_port, zip(range(args.start, args.end), itertools.repeat(args.ssl)))
    pool.close()
    pool.join()